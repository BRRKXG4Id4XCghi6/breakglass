# breakglass

### Encrypt

```
 export PASSPHRASE=$(echo ${PW_SEED} | openssl dgst -sha3-512 | cut -c 10-1000 | base64 -w 0)

rm -rf bg.enc

gpg --verbose --symmetric --batch --passphrase $PASSPHRASE -o bg.enc ./bg.md

cat ./bg.enc | base64 > ./bg_enc.b64

# To send to clipboard
#
# cat ./bg.enc | base64 | xclip -sel clip

```

### Decrypt

```
 export PASSPHRASE=$(echo ${PW_SEED} | openssl dgst -sha3-512 | cut -c 10-1000 | base64 -w 0)

cat ./bg_enc.b64 | base64 -d > ./bg.enc

gpg --verbose --decrypt --batch --passphrase $PASSPHRASE -o bg.md ./bg.enc

```

### Generate QR code
```
qrencode -t ansiutf8 < ./config_file
```

#### Hint
```
Sheffield Aberdeen
```
